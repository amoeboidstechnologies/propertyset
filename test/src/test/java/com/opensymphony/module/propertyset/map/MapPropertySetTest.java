/*
 * Copyright (c) 2002-2003 by OpenSymphony
 * All rights reserved.
 */
package com.opensymphony.module.propertyset.map;

import com.opensymphony.module.propertyset.AbstractPropertySetTest;
import org.junit.Before;

import java.util.HashMap;

import static org.junit.Assert.assertEquals;


/**
 * User: bbulger
 * Date: May 22, 2004
 */
public class MapPropertySetTest extends AbstractPropertySetTest {
    //~ Methods ////////////////////////////////////////////////////////////////

    public void testGetMapSetMap() {
        HashMap map = new HashMap();
        map.put("key1", "value1");
        ((MapPropertySet) ps).setMap(map);
        assertEquals(map, ((MapPropertySet) ps).getMap());
    }

    @Before
    public void setUp() throws Exception {

        HashMap args = new HashMap();
        ps = new MapPropertySet();
        ps.init(null, args);
    }
}
